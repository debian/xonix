.\" 
.\" Copyright (c) 1995
.\"   Joerg Wunsch <joerg_wunsch@uriah.heep.sax.de>
.\" 
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 
.\" THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
.\" IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
.\" OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
.\" IN NO EVENT SHALL THE DEVELOPERS BE LIABLE FOR ANY DIRECT, INDIRECT,
.\" INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
.\" NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
.\" DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
.\" THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
.\" (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
.\" THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
.\" 
.\" xonix.man -- man page for the xonix game
.\"
.\" xonix.man,v 1.7 1995/08/28 10:58:03 j Exp
.\"
.TH XONIX 6
.SH NAME
xonix \- a game
.SH SYNOPSIS
.B xonix
[
.I \-toolkitoption
\&.\|.\|. ]
.SH DESCRIPTION
The \fIxonix\fP application consists of a playing area and a status
display below.

The status display shows the current values for level, filled area
(in percent), number of players (lifes), and elapsed time.

The playing area has several regions.  The brown region (initially
only the border) is ``filled region'', where the player can move,
starting from the top left corner.  Beware of the bouncing yellow
eater(s) that do also move across filled regions however, if they hit
the player, a life will go away.  The flyers can only bounce across
the green (so-called empty) area in the middle.

The purpose of the game is to move the player across the empty region
(whereby it leaves his way in a brown color to show where it came
along), and finally cut off a piece of unfilled region by moving him
back to some filled region.  If the player itself or the (unfilled
yet) way will be hit by a flyer, a life will be lost again.  Once the
player's way reached another part of filled region, the way and all
adjacent unfilled regions where there is no flyer in will be filled in
turn.  One level has completed as soon as 75 % of the originally
unfilled area have been filled this way.

Every level, there will be one flyer more.  Every second level, an
additional player will be granted.  Every fifth level, an additional
eater will be fired off.

The default keys to move the player around are the arrow keys.  This
can be changed in the app-defaults file, however, as well as the
timeout between single steps (defaulting to 50 ms equal 20 moves per
second).

The default keys to immediately quit the game are `Q' and `Escape'.
Hitting `P' or iconizing the window with the window manager will pause
the game; de-iconizing will continue it.

On Posix-compliant systems, there is also a high-score file,
\fB$PROJECTROOT/\fP\fIlib/X11/xonix/scores\fP.  It records the top ten
xonix players for that machine.  As a special compile-time option,
a mail is sent to the previous xonix score leader when he's lost
his first rank.

.SH WIDGETS
\fIXonix\fP uses the X Toolkit and the Athena Widget Set.
Below is the widget structure of the \fIxonix\fP application.
Indentation indicates hierarchical structure.  The widget class
name is given first, followed by the widget instance name.
.sp
.nf
	Xonix  xonix
		VendorShellExt  shellext
		Form  container
			MenuButton  game_button
			Canvas  canvas
			Form  status
				Form  level
					Label  lev_label
					Label  lev_d10
					Label  lev_d1
				Form  percentage
					Label  perc_label
					Label  perc_d10
					Label  perc_d1
				Form  runner
					Label  run_label
					Label  run_d10
					Label  run_d1
				Form  time
					Label  time_label
					Label  mins_d10
					Label  mins_d1
					Label  time_colon
					Label  secs_d10
					Label  secs_d1
			SimpleMenu  game_menu
				SmeBSB  about
				SmeLine  game_l1
				SmeBSB  quit
		TransientShell  about_shell
			VendorShellExt  shellext
			Box  about_box
				Label  about_msg
				Command  about_done
		TransientShell  gameover_shell
			VendorShellExt  shellext
			Box  gameover_box
				Label  gameover_msg
				Box  gameover_buttonbox
					Command  gameover_iknow
					Command  gameover_goon
.fi
.SH OPTIONS
All the standard toolkit options apply.  It's not particularly useful
to attempt forcing any geometry however.

.SH AUTHORS
The original \fIxonix\fP game has been seen somewhere on an old
PC/XT clone.  This is a reimplementation from scratch, done by
.if t Torsten Sch\(:onitz
.if n Torsten Schoenitz
starting the project on a Macintosh.  The X11 support has been written
by
.if t J\(:org Wunsch
.if n Joerg Wunsch
with the pleasant help by
.if t Alfredo Herrera Hern\('andez.
.if n Alfredo Herrera Hernandez.

.SH BUGS
Source code comments are still mostly in German.  Some files require
the unusal tab width of 4 in order to be displayed correctly.

It should be possible to pass some parameters from the command line
as well (e.g. the time step value), which is currently only possible
by the back-door via the \fI-xrm\fP toolkit option.

Mail any suggestions to <joerg_wunsch@uriah.heep.sax.de>.
